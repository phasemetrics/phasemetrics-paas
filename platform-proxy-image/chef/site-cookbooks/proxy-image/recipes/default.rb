#
# Cookbook Name:: proxy-image
# Recipe:: default
#
# Copyright 2016, PhaseMetrics
#
# All rights reserved - Do Not Redistribute
#

bash 'allow_gateway_ports' do
  code 'echo -e "\n## Enable GatewayPorts\nGatewayPorts yes" >> /etc/ssh/sshd_config'
end

bash 'keep_ssh_connections_fresh' do
  code 'echo -e "\n## Keep SSH connections fresh. \nClientAliveInterval 60" >> /etc/ssh/sshd_config'
end

bash 'disable_ssh_welcome_message' do
  code 'chmod -x /etc/update-motd.d/*'
end

