"""Tests for the User model."""
import unittest

from app import (
    create_app,
    db,
    )
from app.models import (
    AnonymousUser,
    Permission,
    Role,
    User,
    )


class UserModelTestCase(unittest.TestCase):
    """Test User model functions."""
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_setter(self):
        """The password setter method works."""
        user = User(password='cat')
        self.assertTrue(user.password_hash is not None)

    def test_no_password_getter(self):
        """No password getter method."""
        user = User(password='cat')
        with self.assertRaises(AttributeError):
            str(user.password)

    def test_password_verification(self):
        """The password verification method works."""
        user = User(password='cat')
        self.assertTrue(user.verify_password('cat'))
        self.assertFalse(user.verify_password('dog'))

    def test_password_salts_are_random(self):
        """Password salts are random."""
        user = User(password='cat')
        user_2 = User(password='cat')
        self.assertTrue(user.password_hash != user_2.password_hash)

    def test_roles_and_permissions(self):
        """Permissions are correctly applied."""
        Role.insert_roles()
        user = User(username='admin', password='cat')
        self.assertTrue(user.can(Permission.VIEW_CONTENT))
        self.assertFalse(user.can(Permission.ADMINISTER_SITE))

    def test_anonymous_user(self):
        """Permissions are correctly applied for anonymous users."""
        user = AnonymousUser()
        self.assertFalse(user.can(Permission.VIEW_CONTENT))
