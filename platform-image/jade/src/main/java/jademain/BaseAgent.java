package jademain;

import jade.content.ContentElement;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.Agent;
import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.QueryPlatformLocationsAction;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.List;

@SuppressWarnings("serial")
public class BaseAgent extends Agent {
	
	protected AMSAgentDescription [] ams_agents() throws FIPAException {
		SearchConstraints constraints = new SearchConstraints();
		constraints.setMaxResults(new Long(-1));
		AMSAgentDescription [] agents =
				AMSService.search(this, new AMSAgentDescription(), constraints);
		return agents;
	}
	
	protected void ams_query() {
		this.getContentManager()
			.registerLanguage(new SLCodec(), FIPANames.ContentLanguage.FIPA_SL);
		this.getContentManager()
			.registerOntology(JADEManagementOntology.getInstance());
		
		QueryPlatformLocationsAction query = new QueryPlatformLocationsAction();
		Action action = new Action(this.getAID(), query);
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.addReceiver(this.getAMS());
		msg.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
		msg.setOntology(JADEManagementOntology.getInstance().getName());
		
		try {
			this.getContentManager().fillContent(msg, action);
			this.send(msg);
		}
		catch (OntologyException e) {
			e.printStackTrace();
		}
		catch (CodecException e) {
			e.printStackTrace();
		}
	}
	
	protected List ams_listen() {
		MessageTemplate mt = MessageTemplate.MatchSender(this.getAMS());
		ACLMessage rcv = this.blockingReceive(mt);
		List list = null;

		try {
			ContentElement content =
					this.getContentManager().extractContent(rcv);
			Result result = (Result) content;
			list = (List) result.getValue();
		}
		catch (OntologyException e) {
			e.printStackTrace();
		}
		catch (CodecException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	protected void registerAgent(String serviceType) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType(serviceType);
		sd.setName(this.getLocalName());
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException e) {
			System.out.println("Agent already registered.");
		}
	}
}
