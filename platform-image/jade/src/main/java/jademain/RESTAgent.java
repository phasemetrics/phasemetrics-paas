package jademain;

import java.io.IOException;
import java.io.OutputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import jade.core.ContainerID;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.util.leap.Iterator;
import jade.util.leap.List;
import jademain.annotations.GET;
import jademain.annotations.Path;

@SuppressWarnings("serial")
public class RESTAgent extends RESTBaseAgent {	
	public void setup() {		
		this.registerAgent("RESTful Server");
		this.startServer();
	}

	@GET
	@Path("/api/agents")
	public void api_agent(OutputStream response) throws IOException, FIPAException {
		JSONObject j = new JSONObject();
		JSONArray ja = new JSONArray();
		
		AMSAgentDescription [] agents = this.ams_agents();
		for (int i = 0; i < agents.length; i++) {
			if (!agents[i].getName().getLocalName().equals("RESTServer")) {
				JSONObject agent = new JSONObject();
				agent.put("Name", agents[i].getName().getName());
				agent.put("LocalName", agents[i].getName().getLocalName());
				ja.put(agent);
			}
		}
		
		j.put("agents", ja);
		response.write(j.toString().getBytes());
		response.close();
	}

	@GET
	@Path("/api/containers")
	public void api_container(OutputStream response) throws IOException {
		JSONObject j = new JSONObject();
		JSONArray ja = new JSONArray();
		
		this.ams_query();
		List list = this.ams_listen();
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			ContainerID next = (ContainerID) iter.next();
			JSONObject container = new JSONObject();
			container.put("Name", next.getName());
			container.put("ID", next.getID());
			ja.put(container);
		}
		
		j.put("containers", ja);
		response.write(j.toString().getBytes());
		response.close();
	}
}
