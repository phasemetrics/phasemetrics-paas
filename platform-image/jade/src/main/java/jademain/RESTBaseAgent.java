package jademain;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;

import org.json.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import jademain.annotations.Path;

@SuppressWarnings("serial")
public class RESTBaseAgent extends BaseAgent implements HttpHandler {
	
	private static final Integer HTTP_PORT = 5001;
	
	protected void startServer() {
		try {
			HttpServer server = HttpServer.create(new InetSocketAddress(HTTP_PORT), 0);
			server.createContext("/api/", this);
			server.setExecutor(null);
			server.start();
			System.out.println(String.format("RESTAgent is now listening at %d.", HTTP_PORT));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void handle(HttpExchange exchange) throws IOException {
		String uri = exchange.getRequestURI().toString();
		Class<? extends Annotation> methodClass;
		Boolean responded = false;
		
		try {
			methodClass = (Class<? extends Annotation>) Class.forName(
					"jademain.annotations.".concat(exchange.getRequestMethod()));
			
			Method[] methods = this.getClass().getMethods();

			for (Method method : methods){
				if (method.isAnnotationPresent(methodClass)) {
					if (method.isAnnotationPresent(Path.class)) {
						Path path = method.getAnnotation(Path.class);
						String mpath = path.value();

						if (mpath.trim().equals(uri.trim())) {
							Headers responseHeaders = exchange.getResponseHeaders();
							responseHeaders.set("Content-Type", "application/json");
							exchange.sendResponseHeaders(200, 0);
							method.invoke(this, exchange.getResponseBody());
							responded = true;
						}
					}
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			this.badRequest(exchange);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			this.badRequest(exchange);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			this.badRequest(exchange);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			this.badRequest(exchange);
		}
		
		if (!responded) this.badRequest(exchange);
	}
	
	protected void badRequest(HttpExchange exchange) throws IOException {
		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "application/json");
		exchange.sendResponseHeaders(400, 0);		
		JSONObject j = new JSONObject();
		j.put("Error", "Bad Request");		
		OutputStream responseBody = exchange.getResponseBody();
		responseBody.write(j.toString().getBytes());
		responseBody.close();
	}
}
