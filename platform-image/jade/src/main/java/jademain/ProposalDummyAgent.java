package jademain;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.ProposeInitiator;

@SuppressWarnings("serial")
public class ProposalDummyAgent extends Agent {
	@Override
	protected void setup() {
		addBehaviour(new TickerBehaviour(this, 5000) {			
			@Override
			protected void onTick() {
				ACLMessage init = new ACLMessage(ACLMessage.PROPOSE);
				init.addReceiver(new AID("Dragnet", AID.ISLOCALNAME));
				init.setContent((String) myAgent.getArguments()[0]);
				getAgent().addBehaviour(new ProposeInitiator(myAgent, init));
			}
		});
	}
}
