package services;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.AgentContainer;
import jade.core.BaseService;
import jade.core.Filter;
import jade.core.Profile;
import jade.core.ProfileException;
import jade.core.VerticalCommand;
import jade.security.StateNotificationSlice;

public class PhaseMetricsManagementService extends BaseService {
	
	@SuppressWarnings("serial")
	private List<String> trusted =  new ArrayList<String>(){{
		add("ams");
		add("df");
		add("RESTServer");
	}};
	
	private AgentContainer myContainer;	
	private Filter inFilter;
	
	@Override
	public void init(AgentContainer ac, Profile p) throws ProfileException {
		super.init(ac, p);
		
		myContainer = ac;
		inFilter = new InFilter();
	}
	
	@Override
	public String getName() {
		return PhaseMetricsManagementSlice.NAME;
	}
	
    @Override
    public Filter getCommandFilter(boolean direction) {
    	if (direction == Filter.INCOMING) {
    		return inFilter;
    	} else {
			return null;
		}
    }

	private class InFilter extends Filter {
		@Override
		protected boolean accept(VerticalCommand cmd) {			
			if (cmd.getName().equals(StateNotificationSlice.REQUEST_AUTH)) {
				AID aid = (AID) cmd.getParam(0);
				if (trusted.contains(aid.getLocalName())) return true;
				Agent agent= myContainer.acquireLocalAgent(aid);
				
				myContainer.releaseLocalAgent(aid);
			}
			return true;
		}
	}
}
