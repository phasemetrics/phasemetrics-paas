#
# Cookbook Name:: platform-image
# Recipe:: default
#
# Copyright 2016, VorsoLabs
#
# All rights reserved - Do Not Redistribute
#

USER = 'ubuntu'

jade_directories = %w(
  main
  backup
  backup-2
  container
)

jade_directories.each do |jade_dir|
  directory '/home/' + USER + '/' + jade_dir do
    owner USER
    group USER
    mode '0755'
    action :create
  end
end

bash 'move_leap_properties' do
  code "mv /home/" + USER + '/leap.properties /home/' + USER + '/container/'
end

directory '/var/jadeplatform' do
  owner USER
  group USER
  mode '0755'
  action :create
end

file '/var/jadeplatform/ports' do
  owner USER
  group USER
  mode '0755'
  action :create
end

template '/lib/systemd/system/jadeplatform-clear.service' do
  source 'systemd-clear.conf.erb'
  notifies :enable, 'service[jadeplatform-clear]'
end

template '/lib/systemd/system/jadeplatform-main.service' do
  source 'systemd-main.conf.erb'
  variables ({
    :mac_user => USER,
    :script_name => 'PhaseMetricsA',
    :script_services => ''
  })
  notifies :enable, 'service[jadeplatform-main]'
end

template '/lib/systemd/system/jadeplatform-backup.service' do
  source 'systemd-backup.conf.erb'
  variables ({
    :mac_user => USER,
    :script_name => 'PhaseMetricsB',
    :script_services => ''
  })
  notifies :enable, 'service[jadeplatform-backup]'
end

template '/lib/systemd/system/jadeplatform-backup-2.service' do
  source 'systemd-backup-2.conf.erb'
  variables ({
    :mac_user => USER,
    :script_name => 'PhaseMetricsC',
    :script_services => ''
  })
  notifies :enable, 'service[jadeplatform-backup-2]'
end

template '/lib/systemd/system/jadeplatform-container.service' do
  source 'systemd-container.conf.erb'
  variables ({
    :mac_user => USER,
    :script_services => ';jade.security.StateNotificationService',
    :script_agents => '"RESTServer:jademain.RESTAgent"'
  })
  notifies :enable, 'service[jadeplatform-container]'
end

service 'jadeplatform-clear' do
  provider Chef::Provider::Service::Systemd
  supports :start => true, :restart => true, :stop => true, :reload => true
  action :nothing 
end

service 'jadeplatform-main' do
  provider Chef::Provider::Service::Systemd
  supports :start => true, :restart => true, :stop => true, :reload => true
  action :nothing 
end

service 'jadeplatform-backup' do
  provider Chef::Provider::Service::Systemd
  supports :start => true, :restart => true, :stop => true, :reload => true
  action :nothing 
end

service 'jadeplatform-backup-2' do
  provider Chef::Provider::Service::Systemd
  supports :start => true, :restart => true, :stop => true, :reload => true
  action :nothing 
end

service 'jadeplatform-container' do
  provider Chef::Provider::Service::Systemd
  supports :start => true, :restart => true, :stop => true, :reload => true
  action :nothing 
end