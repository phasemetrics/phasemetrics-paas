name             'phasemetrics'
maintainer       'VorsoLabs'
maintainer_email 'sando.george@vorsolabs.com'
license          'All rights reserved'
description      'Installs/Configures phasemetrics'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'ssh_known_hosts'
depends 'magic_shell'