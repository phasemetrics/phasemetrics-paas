#
# Cookbook Name:: phasemetrics
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

USER = 'ubuntu'
APP_HOME = '/srv/www/phasemetrics'

assets_file = File.read('/home/' + USER + '/chef-assets/assets.json')
asset_hash = JSON.parse(assets_file)

prefix = 'PHASEMETRICS_ADMIN=\'' + asset_hash['app_admin'] +
          '\' PHASEMETRICS_ADMIN_PASSWORD=\'' + asset_hash['app_admin_password'] +
          '\' PHASEMETRICS_DEV_DATABASE_URL=' + asset_hash['dev_db_url']

## AWS Credentials.
magic_shell_environment 'AWS_ACCESS_KEY_ID' do
  value ENV['AWS_ACCESS_KEY_ID']
end

magic_shell_environment 'AWS_SECRET_ACCESS_KEY' do
  value ENV['AWS_SECRET_ACCESS_KEY']
end

magic_shell_environment 'AWS_DEFAULT_REGION' do
  value ENV['AWS_DEFAULT_REGION']
end

magic_shell_environment 'PHASEMETRICS_ADMIN' do
  value asset_hash['app_admin']
end

magic_shell_environment 'PHASEMETRICS_ADMIN_PASSWORD' do
  value asset_hash['app_admin_password']
end

magic_shell_environment 'PHASEMETRICS_DEV_DATABASE_URL' do
  value asset_hash['dev_db_url']
end

## Install virtualenv
package 'python3-pip'
package 'python3-setuptools'
package 'python3.5-dev'
package 'python-virtualenv'

## Install uwsgi
package 'uwsgi'
package 'uwsgi-plugin-python3'

## Intsall lessCSS
package 'node-less'

## Install libffi-dev and libssl-dev that's used when installing paramiko with pip.
package 'libssl-dev'
package 'libffi-dev'

## Install memcached library and server.
package 'libmemcached-dev'
package 'memcached'

## Configure Bitbucket Credentials for USER
ssh_known_hosts_entry 'bitbucket.org'

template '/home/' + USER + '/.ssh/config' do
  source 'ssh-config.erb'
  mode '0775'
  owner USER
  group USER
  variables({
    :user => USER
  })
  guard_interpreter :bash
  not_if '[[ $(grep -i bitbucket.org /home/' + USER + '/.ssh/config) ]]'
end

## Create and Set Permissions for Application Folder
directory '/srv/www/' do
  owner USER
  group 'deployment'
  mode '0775'
  action :create
end

## Checkout PaaS code to /srv/www/phasemetrics
git APP_HOME do
  repository 'git@bitbucket.org:phasemetrics/phasemetrics-paas.git'
  revision 'HEAD'
  reference 'master'
  environment 'HOME' => '/home/' + USER
  user USER
  group 'deployment'
  action :checkout
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics ]]'
end

## Set the Deploy Branch to Track origin/master
execute 'track_master' do
  command 'git branch --set-upstream-to=origin/master deploy'
  user USER
  cwd APP_HOME
end

## Create Virtual Environment and Install Requirements
execute 'virtualenv' do
  command 'virtualenv -p python3.5 venv'
  user USER
  cwd APP_HOME
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics/venv ]]'
end

bash 'install_python_requirements' do
  code 'source venv/bin/activate; pip install -r requirements.txt; deactivate'
  user USER
  cwd APP_HOME
  environment ({ 'HOME' => ::Dir.home(USER), 'USER' => USER })
end

## Fix webassets path permissions.
bash 'fix_webassets_path_permissions' do
  code 'chmod 0777 -R app/static/.webassets-cache; chmod 0777 -R app/static/gen'
  cwd APP_HOME
end

## Initialize database.
bash 'initialize_database' do
  code 'source venv/bin/activate;' + prefix + ' python manage.py initdb ; deactivate'
  user USER
  cwd APP_HOME
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics/migrations ]]'
end

## Insert Roles and Admin User.
bash 'insert_roles_and_users' do
  code 'source venv/bin/activate;' + prefix + ' python manage.py create-admin ; deactivate'
  user USER
  cwd APP_HOME
  guard_interpreter :bash
  only_if '[[ -d /srv/www/phasemetrics/migrations ]]'
end

## Run installation command.
bash 'do_install' do
  code 'source venv/bin/activate;' + prefix + ' python manage.py do-install ; deactivate'
  user USER
  cwd APP_HOME
  guard_interpreter :bash
  only_if '[[ -d /srv/www/phasemetrics/migrations ]]'
end

## Configure uWSGI
template '/etc/uwsgi/apps-available/phasemetrics.ini' do
  source 'uwsgi.erb'
  mode '0440'
  owner 'root'
  group 'root'
  variables({
    :app_admin => asset_hash['app_admin'],
    :app_admin_password => asset_hash['app_admin_password'],
    :app_config => asset_hash['app_config'],
    :app_prefix => 'PHASEMETRICS',
    :base => '/srv/www/phasemetrics',
    :db_url => asset_hash['dev_db_url'],
    :mail_server => asset_hash['mail_server'],
    :mail_port => asset_hash['mail_port'],
    :mail_use_tls => asset_hash['mail_use_tls'],
    :mail_use_ssl => asset_hash['mail_use_ssl'],
    :mail_username => asset_hash['mail_username'],
    :mail_password => asset_hash['mail_password'],
    :mail_default_sender => asset_hash['mail_default_sender'],
    :ga_tracking_id => asset_hash['ga_tracking_id'],
    :secret_key => asset_hash['app_secret_key'],
    :socket_name => 'phasemetrics',
    :aws_access_key_id => ENV['AWS_ACCESS_KEY_ID'],
    :aws_secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'],
    :aws_default_region => ENV['AWS_DEFAULT_REGION']
  })
end

bash 'enable_app' do
  code 'ln -s /etc/uwsgi/apps-available/phasemetrics.ini /etc/uwsgi/apps-enabled/phasemetrics.ini'
  guard_interpreter :bash
  not_if '[[ -f /etc/uwsgi/apps-enabled/phasemetrics.ini ]]'
end

execute 'service uwsgi restart'

## Configure NGINX
template '/etc/nginx/sites-available/phasemetrics.io' do
  source 'nginx.erb'
  mode '0440'
  owner 'root'
  group 'root'
  variables({
    :location => 'phasemetrics',
    :socket_name => 'phasemetrics',
    :server_name => 'phasemetrics.io'
  })
end

bash 'enable_site' do
  code 'ln -s /etc/nginx/sites-available/phasemetrics.io /etc/nginx/sites-enabled/phasemetrics.io'
  guard_interpreter :bash
  not_if '[[ -f /etc/nginx/sites-enabled/phasemetrics.io ]]'
end

bash 'remove_default_enabled' do
  code 'rm /etc/nginx/sites-enabled/000-default'
  guard_interpreter :bash
  only_if '[[ -f /etc/nginx/sites-enabled/000-default ]]'
end

bash 'remove_default_available' do
  code 'rm /etc/nginx/sites-available/default'
  guard_interpreter :bash
  only_if '[[ -f /etc/nginx/sites-available/default ]]'
end

execute 'service nginx restart'


directory '/var/jadeplatform' do
  action :create
end

bash 'copy_jadeplatform_key' do
  code 'cp /home/' + USER + '/.ssh/JADEPlatform.pem /var/jadeplatform/JADEPlatform.pem'
end

bash 'copy_jadeproxy_key' do
  code 'cp /home/' + USER + '/.ssh/JADEProxy.pem /var/jadeplatform/JADEProxy.pem'
end

bash 'chmod_jadeplatform_key' do
  code 'chmod 0644 /var/jadeplatform/JADEPlatform.pem'
end

bash 'chmod_jadeproxy_key' do
  code 'chmod 0644 /var/jadeplatform/JADEProxy.pem'
end