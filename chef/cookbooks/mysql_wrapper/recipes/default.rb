#
# Cookbook Name:: mysql_wrapper
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

USER = 'ubuntu'

assets_file = File.read('/home/' + USER + '/chef-assets/assets.json')
asset_hash = JSON.parse(assets_file)

## Install mysql2_chef_gem
mysql2_chef_gem 'default' do
  action :install
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics ]]'
end

## Install MySQL as a Service
mysql_service 'default' do
  port '3306'
  version '5.7'
  initial_root_password asset_hash['mysql_root_password']
  action [:create, :start]
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics ]]'
end

## Create Application Database and User
mysql_database 'phasemetrics' do
  connection(
    :host => '127.0.0.1',
    :username => 'root',
    :password => asset_hash['mysql_root_password']
  )
  action :create
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics ]]'
end

mysql_database_user 'phasemetrics' do
  connection(
    :host => '127.0.0.1',
    :username => 'root',
    :password => asset_hash['mysql_root_password']
  )
  password asset_hash['app_mysql_password']
  database_name 'phasemetrics'
  action :grant
  guard_interpreter :bash
  not_if '[[ -d /srv/www/phasemetrics ]]'
end