#
# Cookbook Name:: cookinit
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

USER = 'ubuntu'

## Create Deployment Group
group 'deployment' do
  members [USER]
  action :create
  guard_interpreter :bash
  not_if '[[ $(grep deployment /etc/group) ]]'
end