#
# Cookbook Name:: jenkins_wrapper
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

USER = 'ubuntu'

assets_file = File.read('/home/' + USER + '/chef-assets/assets.json')
asset_hash = JSON.parse(assets_file)

## Install Jenkins Plugins
jenkins_update_plugins = %w(
  script-security=latest
  antisamy-markup-formatter=latest
  windows-slaves=latest
  ssh-slaves=latest
  ssh-credentials=latest
  javadoc=latest
  pam-auth=latest
  cvs=latest
  external-monitor-job=latest
  translation=latest
  mailer=latest
  ldap=latest
  ant=latest
  junit=latest
  matrix-project=latest
  maven-plugin=latest
  subversion=latest
  credentials=latest
  bouncycastle-api=latest
  matrix-auth=latest
  icon-shim=latest
  structs=latest
  display-url-api=latest
  mercurial=latest
  scm-api=latest
  workflow-scm-step=latest
  workflow-step-api=latest
  mapdb-api=latest
)

jenkins_update_plugins.each do |addon|
  name, ver = addon.split('=')
  jenkins_plugin name do
    version ver
    install_deps false
    guard_interpreter :bash
    not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
  end
end

jenkins_command 'safe-restart' do
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

jenkins_new_plugins = %w(
  git=latest
  bitbucket=latest
  job-dsl=latest
)

jenkins_new_plugins.each do |addon|
  name, ver = addon.split('=')
  jenkins_plugin name do
    version ver
    install_deps false
    guard_interpreter :bash
    not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
  end
end

jenkins_command 'safe-restart' do
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

jenkins_plugin 'git-client' do
  version 'latest'
  install_deps false
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

jenkins_command 'safe-restart' do
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

## Configure Credentials
jenkins_private_key_credentials asset_hash['jenkins_pkey_cred'] do
  id asset_hash['jenkins_pkey_id']
  description asset_hash['jenkins_pkey_cred_desc']
  private_key asset_hash['jenkins_pkey_cred_key']
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

## Configure Users
jenkins_user asset_hash['jenkins_admin'] do
  password asset_hash['jenkins_admin_pwd']
  full_name asset_hash['jenkins_admin_name']
  email asset_hash['jenkins_admin_email']
  public_keys asset_hash['jenkins_admin_keys']
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

## Set the Private Key on the Jenkins Executor
node.run_state[:jenkins_private_key] = asset_hash['jenkins_pkey_cred_key']

## Create Jobs
pp_filename = File.join(Chef::Config[:file_cache_path],'phasemetrics-pass-job.xml')

# PhaseMetrics PaaS
template pp_filename do
  source 'phasemetrics-paas-job.xml.erb'
  variables({
    :credential_id => asset_hash['jenkins_pkey_id']
  })
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

jenkins_job 'phasemetrics-pass' do
  config pp_filename
  action :create
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

## Enable Authentication
jenkins_script 'add_authentication' do
  command <<-EOH.gsub(/^ {4}/, '')
    import jenkins.model.*
    import hudson.security.*
    
    def instance = Jenkins.getInstance()
    
    def hudsonRealm = new HudsonPrivateSecurityRealm(false)
    instance.setSecurityRealm(hudsonRealm)
    def strategy = new GlobalMatrixAuthorizationStrategy()
    strategy.add(Jenkins.ADMINISTER, '#{asset_hash['jenkins_admin']}')
    instance.setAuthorizationStrategy(strategy)
    instance.save()
  EOH
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

## Enable Slave to Master Access Control
jenkins_script 'slave_master_access_control' do
  command <<-EOH.gsub(/^ {4}/, '')
    import jenkins.model.*
    import jenkins.security.s2m.*    
    Jenkins.instance.injector.getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false);
    Jenkins.instance.save()
  EOH
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

jenkins_command 'safe-restart' do
  guard_interpreter :bash
  not_if '[[ -f /home/' + USER + '/jenkins_configured ]]'
end

file '/home/' + USER + '/jenkins_configured' do
  action :touch
end