#!/bin/bash
ln -sf /usr/share/zoneinfo/US/Eastern /etc/localtime
apt update && apt upgrade -y -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold


if type -p java; then
    echo found java executable in PATH
    _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    echo found java executable in JAVA_HOME     
    _java="$JAVA_HOME/bin/java"
else
    apt install -y python-software-properties debconf-utils
    add-apt-repository -y ppa:webupd8team/java
    apt update
    echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
    apt install -y oracle-java8-installer
fi