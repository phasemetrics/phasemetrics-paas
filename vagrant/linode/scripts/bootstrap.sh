#!/bin/bash
echo "phasemetrics" > /etc/hostname
hostname -F /etc/hostname
ip=$(ip addr show eth0 | grep -Po 'inet \K[\d.]+')
echo "$ip    $ip phasemetrics" >> /etc/hosts
ln -sf /usr/share/zoneinfo/CET /etc/localtime
apt-get update && apt-get upgrade -y -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold