#!/usr/bin/env bash

echo 'Running deployment script'

## Go to application directory.
cd /srv/www/phasemetrics

## Issue git pull command to get new code from repository.
git pull

## Update Python requirements.
source venv/bin/activate && pip install -r requirements.txt

## Restart uWSGI
sudo service uwsgi restart

## Go back to workspace.
cd ~/

echo 'Deployment complete.'