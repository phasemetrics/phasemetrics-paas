"""Implements the project ui."""

from flask import Blueprint

project = Blueprint('project', __name__)

from . import (
    forms,
    views,
    )

__all__ = ['project']