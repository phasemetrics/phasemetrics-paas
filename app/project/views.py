"""Implements project endpoints."""

from datetime import datetime
import random
import re

from flask import (
    current_app, flash, redirect, render_template,
    request,
)
from flask_breadcrumbs import register_breadcrumb
from flask_login import (
    current_user, login_required,
)
import paramiko

from . import project
from .forms import (
    APIKeyForm, EditProjectForm, NewPlatformProxyForm, NewProjectForm,
)
from app import (
    cache, db, ec2client, ec2resource, sshclient
)
from app.confirm import confirm_required
from app.utils.constructors import (
    edit_project, view_project, project_add_api_key,
)
from app.utils.decorators import (
    confirmed_required,
)
from app.utils.platform_wrapper import AgentPlatformWrapper
from app.utils.models import (
    APIKey, Platform, PlatformProxy, Project,
)
from flask.helpers import url_for


@project.route('/')
@login_required
@confirmed_required
@register_breadcrumb(project, 'breadcrumbs.project', 'Projects')
def index():
    """Render index page for the project context."""
    projects = Project.query.filter_by(owner_id=current_user.id).all()
    page_vars = {
        'sidebar_class': ' no-sidebars',
        'navwell': True,
        'title': 'Manage Your Projects',
        'page_header': 'Projects',
        'projects': projects
    }
    return render_template('project/index.html', vars=page_vars)


@project.route('/add', methods=['GET', 'POST'])
@login_required
@confirmed_required
@register_breadcrumb(project, 'breadcrumbs.project.add', 'New Project')
def add():
    """Present form to add a project"""
    form = NewProjectForm()

    if form.validate_on_submit():
        p = Project(
            name=form.name.data,
            owner_id=current_user.id,
            created=datetime.now()
        )
        db.session.add(p)
        db.session.commit()
        return redirect(url_for('project.view', pid=p.id))

    page_vars = {
        'sidebar_class': ' no-sidebars',
        'navwell': True,
        'title': 'Create a New Project',
        'page_header': 'New Project',
        'form': form
    }
    return render_template('project/add.html', vars=page_vars)


@project.route('/<int:pid>')
@login_required
@confirmed_required
@register_breadcrumb(project, 'breadcrumbs.project.view', '', dynamic_list_constructor=view_project)
def view(pid):
    """View a project."""
    p = Project.query.filter_by(id=pid).first()
    if p is None or (current_user.id != p.owner_id):
        return render_template('error/404.html', vars={}), 404

    instance = None
    agents = None
    jade_up = False
    platform = p.platform.first()
    if platform is None:
        pass
    else:
        instance = ec2resource.Instance(platform.instance_id)
        apw = AgentPlatformWrapper(instance.public_ip_address)
        if apw.is_up():
            jade_up = True
            agents = apw.api_agents()

    proxy_instance = None
    proxy = p.proxy.first()
    if proxy is not None:
        proxy_instance = ec2resource.Instance(proxy.instance_id)

    keys = p.api_keys.all()

    page_vars = {
        'sidebar_class': ' sidebar' if platform is not None else ' no-sidebars',
        'sidebar_right': True if platform is not None else False,
        'navwell': True,
        'title': p.name,
        'page_header': p.name,
        'project_title': p.name,
        'pid': p.id,
        'platform': True if platform is not None else False,
        'instance': instance,
        'proxy': True if proxy is not None else False,
        'proxy_instance': proxy_instance,
        'api_keys': keys,
        'agents': agents,
        'jade_up': 'Up' if jade_up else 'Down'
    }
    return render_template('project/view.html', vars=page_vars)


@project.route('/<int:pid>/edit', methods=['GET', 'POST'])
@login_required
@confirmed_required
@register_breadcrumb(project, 'breadcrumbs.project.edit', '', dynamic_list_constructor=edit_project)
def edit(pid):
    """Edit a project."""
    p = Project.query.filter_by(id=pid).first()
    if p is None or (current_user.id != p.owner_id):
        return render_template('error/404.html', vars={}), 404

    form = EditProjectForm()
    if form.validate_on_submit():
        temp = p.name
        p.name = form.name.data

        db.session.add(p)
        db.session.commit()

        flash('Project "{0}" has been updated.'
              .format(temp), 'success')
        return redirect(url_for('project.view', pid=p.id))

    form.name.data = p.name
    page_vars = {
        'sidebar_class': ' no-sidebars',
        'navwell': True,
        'title': p.name,
        'page_header': 'Edit ' + p.name,
        'pid': p.id,
        'form': form
    }
    return render_template('project/edit.html', vars=page_vars)


@project.route('/<int:pid>/delete')
@login_required
@confirmed_required
@confirm_required(
    'Are you sure you want to delete this project?',
    'I am sure', 'project.view', ['pid'], 'danger'
)
def delete(pid):
    """Delete a project."""
    p = Project.query.filter_by(id=pid).first()
    if p is None or (current_user.id != p.owner_id):
        return render_template('error/404.html', vars={}), 404

    plt = p.platform.first()
    if plt is not None:
        instance = ec2resource.Instance(plt.instance_id)
        ec2client.disassociate_address(AssociationId=plt.eip_assoc_id)
        ec2client.release_address(AllocationId=plt.eip_alloc_id)
        instance.terminate()
        db.session.delete(plt)

    proxy = p.proxy.first()
    if proxy is not None:
        proxy_instance = ec2resource.Instance(proxy.instance_id)
        ec2client.disassociate_address(AssociationId=proxy.eip_assoc_id)
        ec2client.release_address(AllocationId=proxy.eip_alloc_id)
        proxy_instance.terminate()
        db.session.delete(proxy)

    keys = p.api_keys.all()
    for key in keys:
        db.session.delete(key)

    db.session.delete(p)
    db.session.commit()

    flash('Project "{0}" has been deleted.'
          .format(p.name), 'success')
    return redirect(url_for('project.index'))


@project.route('/<int:pid>/platform/add')
@login_required
@confirmed_required
@confirm_required(
    'Are you sure you want to add a new platform to this project?',
    'I am sure', 'project.view', ['pid'], 'warning'
)
def add_platform(pid):

    p = Project.query.filter_by(id=pid).first()

    vpc = list(ec2resource.vpcs.filter(Filters=[{
        'Name': 'tag:Name',
        'Values': [current_app.config['AWS_JADEVPC_NAME']]
    }]))[0]

    subnet = random.choice(
        list(filter(lambda x: x.available_ip_address_count > 0, list(
            ec2resource.subnets.filter(Filters=[{
                'Name': 'vpc-id',
                'Values': [vpc.vpc_id]
            }])
        )))
    )

    image = list(ec2resource.images.filter(Filters=[{
        'Name': 'tag:Name',
        'Values': ['jade-platform-{0}'.format(current_app.config['AWS_JADEPLATFORM_VERSION'])]
    }]))[0]

    security_group = list(ec2resource.security_groups.filter(Filters=[{
        'Name': 'group-name',
        'Values': [current_app.config['AWS_JADEVPC_SECURITYGROUP']]
    }]))[0]

    instances = ec2resource.create_instances(
        ImageId=image.image_id,
        MinCount=1,
        MaxCount=1,
        KeyName=current_app.config['AWS_JADEVPC_KEYNAME'],
        SecurityGroupIds=[
            security_group.group_id
        ],
        UserData=render_template('project/aws-platform-userdata.txt'),
        InstanceType='t2.small',
        SubnetId=subnet.subnet_id
    )

    if len(instances) > 0:
        instance = instances[0]
        instance.wait_until_running()
        instance.load()
        eip = ec2client.allocate_address(Domain='vpc')
        aid = ec2client.associate_address(
            AllocationId=eip['AllocationId'],
            InstanceId=instance.instance_id,
            PrivateIpAddress=instance.private_ip_address
        )

        instance.create_tags(
            Tags=[
                {
                    'Key': 'Name',
                    'Value': 'jade-{0}-{1}'.format(p.owner.username, p.name)
                }
            ]
        )

        platform = Platform(
            secret=Platform.generate_secret(),
            project_id=pid,
            instance_id=instance.instance_id,
            ip_address=eip['PublicIp'],
            eip_alloc_id=eip['AllocationId'],
            eip_assoc_id=aid['AssociationId']
        )
        db.session.add(platform)
        db.session.commit()
        flash('Agent platform created.', 'success')
    else:
        flash('The Agent Platform could not be created.', 'danger')
    return redirect(url_for('project.view', pid=pid))


@project.route('<int:pid>/platform/logs')
@login_required
@confirmed_required
@register_breadcrumb(project, 'breadcrumbs.project.view.logs', 'Logs')
def platform_logs(pid):
    """Display last 300 lines of each platform process' log."""
    p = Project.query.filter_by(id=pid).first()
    plt = p.platform.first()

    pkey = paramiko.RSAKey.from_private_key_file(
        current_app.config['LOCAL_JADEVPC_KEY'])
    sshclient.connect(
        hostname=plt.ip_address,
        username=current_app.config['JADEVPC_USER'],
        pkey=pkey
    )

    main_log = sshclient.exec_command(
        'journalctl -u jadeplatform-main.service -n 300 --system')[1].read().decode('utf-8')
    main_log = re.sub('\n.+\ssystemd.+', '', main_log)
    main_log = re.sub('\n.+\ssudo.+', '', main_log)
    main_log = re.sub(
        'ip-[0-9]+-[0-9]+-[0-9]+-[0-9]+ bash\[[0-9]+\]', '', main_log)

    backup_log = sshclient.exec_command(
        'journalctl -u jadeplatform-backup.service -n 300')[1].read().decode('utf-8')
    backup_log = re.sub('\n.+\ssystemd.+', '', backup_log)
    backup_log = re.sub('\n.+\ssudo.+', '', backup_log)
    backup_log = re.sub(
        'ip-[0-9]+-[0-9]+-[0-9]+-[0-9]+ bash\[[0-9]+\]', '', backup_log)

    backup2_log = sshclient.exec_command(
        'journalctl -u jadeplatform-backup-2.service -n 300')[1].read().decode('utf-8')
    backup2_log = re.sub('\n.+\ssystemd.+', '', backup2_log)
    backup2_log = re.sub('\n.+\ssudo.+', '', backup2_log)
    backup2_log = re.sub(
        'ip-[0-9]+-[0-9]+-[0-9]+-[0-9]+ bash\[[0-9]+\]', '', backup2_log)

    container_log = sshclient.exec_command(
        'journalctl -u jadeplatform-container.service -n 300')[1].read().decode('utf-8')
    container_log = re.sub('\n.+\ssystemd.+', '', container_log)
    container_log = re.sub('\n.+\ssudo.+', '', container_log)
    container_log = re.sub(
        'ip-[0-9]+-[0-9]+-[0-9]+-[0-9]+ bash\[[0-9]+\]', '', container_log)

    page_vars = {
        'sidebar_class': ' no-sidebars',
        'navwell': True,
        'title': '{0} Logs'.format(p.name),
        'page_header': '{0} Logs'.format(p.name),
        'main_log': main_log,
        'backup_log': backup_log,
        'backup2_log': backup2_log,
        'container_log': container_log
    }
    return render_template('project/logs.html', vars=page_vars)


@project.route('/<int:pid>/platform/delete')
@login_required
@confirmed_required
@confirm_required(
    'Are you sure you want to delete this platform?',
    'I am sure', 'project.view', ['pid'], 'danger'
)
def delete_platform(pid):

    p = Project.query.filter_by(id=pid).first()

    plt = p.platform.first()
    if plt is not None:
        instance = ec2resource.Instance(plt.instance_id)
        ec2client.disassociate_address(AssociationId=plt.eip_assoc_id)
        ec2client.release_address(AllocationId=plt.eip_alloc_id)
        instance.terminate()
        db.session.delete(plt)
        flash('The Agent Platform has been deleted.', 'warning')
    return redirect(url_for('project.view', pid=pid))


@project.route('/<int:pid>/platform-proxy/add', methods=['GET', 'POST'])
@login_required
@confirmed_required
@confirm_required(
    'Are you sure you want to add a new platform proxy to this project?',
    'I am sure', 'project.view', ['pid'], 'warning', False
)
@register_breadcrumb(project, 'breadcrumbs.project.view.add_proxy', 'Add Proxy')
def add_platform_proxy(pid):

    p = Project.query.filter_by(id=pid).first()

    form = NewPlatformProxyForm()

    if form.validate_on_submit():
        vpc = list(ec2resource.vpcs.filter(Filters=[{
            'Name': 'tag:Name',
            'Values': [current_app.config['AWS_JADEPROXYVPC_NAME']]
        }]))[0]

        subnet = random.choice(
            list(filter(lambda x: x.available_ip_address_count > 0, list(
                ec2resource.subnets.filter(Filters=[{
                    'Name': 'vpc-id',
                    'Values': [vpc.vpc_id]
                }])
            )))
        )

        image = list(ec2resource.images.filter(Filters=[{
            'Name': 'tag:Name',
            'Values': ['jade-platform-{0}'.format(current_app.config['AWS_JADEPROXY_VERSION'])]
        }]))[0]

        security_group = list(ec2resource.security_groups.filter(Filters=[{
            'Name': 'group-name',
            'Values': [current_app.config['AWS_JADEPROXYVPC_SECURITYGROUP']]
        }]))[0]

        instances = ec2resource.create_instances(
            ImageId=image.image_id,
            MinCount=1,
            MaxCount=1,
            KeyName=current_app.config['AWS_JADEPROXYVPC_KEYNAME'],
            SecurityGroupIds=[
                security_group.group_id
            ],
            UserData=render_template(
                'project/aws-proxy-userdata.txt',
                username=form.username.data,
                password=form.password.data
            ),
            InstanceType='t2.nano',
            SubnetId=subnet.subnet_id
        )

        if len(instances) > 0:
            instance = instances[0]
            instance.wait_until_running()
            instance.load()
            eip = ec2client.allocate_address(Domain='vpc')
            aid = ec2client.associate_address(
                AllocationId=eip['AllocationId'],
                InstanceId=instance.instance_id,
                PrivateIpAddress=instance.private_ip_address
            )

            instance.create_tags(
                Tags=[
                    {
                        'Key': 'Name',
                        'Value': 'jade-proxy-{0}-{1}'.format(p.owner.username, p.name)
                    }
                ]
            )

            proxy = PlatformProxy(
                project_id=pid,
                instance_id=instance.instance_id,
                ip_address=eip['PublicIp'],
                eip_alloc_id=eip['AllocationId'],
                eip_assoc_id=aid['AssociationId']
            )
            db.session.add(proxy)
            db.session.commit()
            flash('Agent platform proxy created.', 'success')
            cache.delete(request.path)
        else:
            flash('The Agent Platform Proxy could not be created.', 'danger')
        return redirect(url_for('project.view', pid=pid))

    page_vars = {
        'title': 'New Platform Proxy',
        'page_header': 'New Platform Proxy',
        'navwell': True,
        'form': form
    }
    return render_template('project/new-proxy.html', vars=page_vars)


@project.route('/<int:pid>/platform-proxy/delete')
@login_required
@confirmed_required
@confirm_required(
    'Are you sure you want to delete this proxy?',
    'I am sure', 'project.view', ['pid'], 'danger'
)
def delete_platform_proxy(pid):

    p = Project.query.filter_by(id=pid).first()

    proxy = p.proxy.first()
    if proxy is not None:
        proxy_instance = ec2resource.Instance(proxy.instance_id)
        ec2client.disassociate_address(AssociationId=proxy.eip_assoc_id)
        ec2client.release_address(AllocationId=proxy.eip_alloc_id)
        proxy_instance.terminate()
        db.session.delete(proxy)
        flash('The Agent Platform Proxy has been deleted.', 'warning')
    return redirect(url_for('project.view', pid=pid))


@project.route('/<int:pid>/apikey/add', methods=['GET', 'POST'])
@login_required
@confirmed_required
@register_breadcrumb(project, 'breadcrumbs.project.addapikey', '', dynamic_list_constructor=project_add_api_key)
def add_api_key(pid):
    form = APIKeyForm()

    if form.validate_on_submit():
        api_key = APIKey(
            name=form.name.data,
            key=form.key.data,
            created=datetime.now(),
            project_id=pid
        )
        db.session.add(api_key)
        db.session.commit()
        flash('The key `{0}` has been added to this project.'.format(
            api_key.name), 'success')
        return redirect(url_for('project.view', pid=pid))

    form.key.data = APIKey.generate_api_key()
    page_vars = {
        'title': 'Add API Key',
        'navwell': True,
        'form': form,
        'key': form.key.data,
        'pid': pid
    }
    return render_template('project/add-api-key.html', vars=page_vars)


@project.route('/<int:pid>/key/<int:kid>/delete')
@login_required
@confirmed_required
@confirm_required(
    'Are you sure you want to delete this API key?',
    'I am sure', 'project.view', ['pid', 'kid'], 'danger'
)
def delete_api_key(pid, kid):
    """Delete a project."""
    key = APIKey.query.filter_by(id=kid).first()
    db.session.delete(key)
    db.session.commit()

    flash('API Key `{0}` has been deleted.'
          .format(key.name), 'success')
    return redirect(url_for('project.view', pid=pid))
