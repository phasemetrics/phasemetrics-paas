"""Implements forms used in the projects ui."""

from flask_wtf import (
    Form,
)
from wtforms import (
    HiddenField,
    PasswordField,
    StringField,
    SubmitField,
)
from wtforms.validators import (
    Required, EqualTo
)


class NewProjectForm(Form):
    name = StringField('Name', validators=[Required()])
    submit = SubmitField('Create')


class EditProjectForm(Form):
    name = StringField('Name', validators=[Required()])
    submit = SubmitField('Save Changes')


class APIKeyForm(Form):
    key = HiddenField()
    name = StringField('Name', validators=[Required()])
    submit = SubmitField('Save')


class NewPlatformProxyForm(Form):
    username = StringField('Username', validators=[Required()])
    password = PasswordField('Password', validators=[Required()])
    confirm_password = PasswordField('Confirm password', validators=[
        Required(),
        EqualTo('password')
    ])
    submit = SubmitField('Create')
