"""Flask application package: app"""

import os

import boto3
from flask import Flask
from flask_analytics import Analytics
from flask_assets import (
    Bundle,
    Environment,
)
from flask_bootstrap import Bootstrap
from flask_breadcrumbs import Breadcrumbs
from flask_login import LoginManager
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
import paramiko
from werkzeug.contrib.cache import MemcachedCache

from config import CONFIG

__all__ = ['app', 'assets', 'bootstrap', 'breadcrumbs',
           'db', 'moment', 'mailer', 'login_manager']

app = Flask(__name__)

Analytics(app)
assets = Environment()
ec2resource = boto3.resource('ec2')
ec2client = boto3.client('ec2')
breadcrumbs = Breadcrumbs()
bootstrap = Bootstrap()
cache = MemcachedCache(['127.0.0.1:11211'], default_timeout=60)
db = SQLAlchemy()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'
login_manager.login_message = None

mailer = Mail()
moment = Moment()

sshclient = paramiko.SSHClient()


def create_app(config_name):
    """Create the application object.

    Blueprints, which are analogous to modules/plugins, are used to add
    functionality to the application.

    Parameters
    ----------
    config_name : str
        The environment context of the application.

    Returns
    -------
    app : object
    """

    # Create application configuration object.
    app.config.from_object(CONFIG[config_name])

    # Initialize application.
    CONFIG[config_name].init_app(app)
    app.config['ANALYTICS']['GOOGLE_CLASSIC_ANALYTICS'][
        'ACCOUNT'] = app.config['GA_TRACKING_ID']
    bootstrap.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    mailer.init_app(app)

    # Register Blueprints.
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .account import account as account_blueprint
    app.register_blueprint(account_blueprint, url_prefix='/account')

    from .api_v1_0 import api as api_v1_0_blueprint
    app.register_blueprint(api_v1_0_blueprint, url_prefix='/api/v1.0')

    from .config import config as config_blueprint
    app.register_blueprint(config_blueprint, url_prefix='/config')

    from .confirm import confirm as confirm_blueprint
    app.register_blueprint(confirm_blueprint, url_prefix='/confirm')

    from .project import project as project_blueprint
    app.register_blueprint(project_blueprint, url_prefix='/projects')

    # Breadcrumbs
    breadcrumbs.init_app(app)

    # Register assets
    assets.init_app(app)

    app_js = Bundle(
        'js/script.js',
        filters='closure_js',
        output='gen/packed.js'
    )
    assets.register('app_js', app_js)

    app_css = Bundle(
        'less/style.less',
        filters='less,yui_css',
        output='gen/packed.css'
    )
    assets.register('app_css', app_css)

    sshclient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    return app


def get_app_prefix():
    """Return the application prefix."""
    return 'PHASEMETRICS'
