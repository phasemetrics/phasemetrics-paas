"""Implements forms used in the confirmation ui."""

from flask_wtf import Form
from wtforms import (
    BooleanField, SubmitField,
)


class ConfirmationForm(Form):
    """Confirm that a user wants to complete a given action."""
    confirmed = BooleanField('Yes')
    submit = SubmitField('Continue')