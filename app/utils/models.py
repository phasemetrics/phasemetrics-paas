"""Object models used throughout the Flask app."""

import os
import string
import uuid
import hashlib

from datetime import datetime

from flask import current_app
from flask_login import (
    UserMixin,
    AnonymousUserMixin,
)

from itsdangerous import (
    BadSignature,
    URLSafeTimedSerializer,
)

from werkzeug.security import (
    generate_password_hash,
    check_password_hash,
)

from app import (
    db,
    login_manager,
)


class Permission(object):
    """Define user roles which are available in the application."""
    VIEW_CONTENT = 0x01
    CREATE_CONTENT = 0x04
    ADMINISTER_SITE = 0x80


class Role(db.Model):
    """Database model for a role."""
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name

    @staticmethod
    def insert_roles():
        """Populate the roles table."""
        roles = {
            'User': (Permission.VIEW_CONTENT, True),
            'Site Manager': (Permission.VIEW_CONTENT |
                             Permission.CREATE_CONTENT, False),
            'Administrator': (0xff, False)
        }
        for item in roles:
            role = Role.query.filter_by(name=item).first()
            if role is None:
                role = Role(name=item)
            role.permissions = roles[item][0]
            role.default = roles[item][1]
            db.session.add(role)
        db.session.commit()


class User(UserMixin, db.Model):
    """Database model for a user."""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), unique=True, index=True)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    init = db.Column(db.String(64), index=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'),
                        server_default='0', nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime, nullable=True)
    active = db.Column(db.Boolean, index=True, nullable=False, default=True)
    deactivated_on = db.Column(db.DateTime, nullable=True)
    projects = db.relationship('Project', backref='user', lazy='dynamic')
    user_invitations = db.relationship(
        'UserInvitation', backref='user', lazy='dynamic')

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['_ADMIN']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def __repr__(self):
        return '<User %r>' % self.username

    @staticmethod
    def insert_admin_user():
        """Create the admin user and insert into the database."""
        admin_user = User.query.filter_by(id=1).first()
        if admin_user is None:
            admin_user = User(username='admin',
                              email=current_app.config['_ADMIN'],
                              init=current_app.config['_ADMIN'],
                              password=current_app.config['_ADMIN_PASSWORD'],
                              registered_on=datetime.now())
            admin_user.role = Role.query.filter_by(permissions=0xff).first()
            db.session.add(admin_user)
            db.session.commit()

    @property
    @staticmethod
    def password():
        """Password not exposed."""
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """Generate hash based on password property."""
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Check supplied string against password hash"""
        return check_password_hash(self.password_hash, password)

    def can(self, permissions):
        """Check user permissions."""
        return self.role is not None and \
            (self.role.permissions & permissions) == permissions

    def is_administrator(self):
        """Check user is administrator"""
        return self.can(Permission.ADMINISTER_SITE)

    def super_user(self):
        """Check if user is the site super user."""
        return self.id == 1

    def generate_security_token(self):
        """Generate a security token."""
        serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        return serializer.dumps(self.email,
                                salt=current_app.config[
                                    'SECURITY_PASSWORD_SALT'
                                ])

    def confirm_security_token(self, token, max_age=3600):
        """Validate a security token."""
        serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        try:
            self.email = serializer.loads(
                token,
                salt=current_app.config['SECURITY_PASSWORD_SALT'],
                max_age=max_age
            )
        except BadSignature:
            raise
        return self.email

    @staticmethod
    def confirm_password_reset_token(token, max_age=3600):
        """Validate password reset token."""
        serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        try:
            email = serializer.loads(
                token,
                salt=current_app.config['SECURITY_PASSWORD_SALT'],
                max_age=max_age
            )
        except BadSignature:
            raise
        return email


class AnonymousUser(AnonymousUserMixin):
    """Define object for anonymous users."""
    @staticmethod
    def can(dummy_permissions):
        """Check permissions of anonymous user."""
        return False

    @staticmethod
    def is_administrator():
        """Check anonymous user is administrator."""
        return False

login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    """Fetch user object from database."""
    return User.query.get(int(user_id))


class UserInvitation(db.Model):
    """Database model for a user invitation."""
    __tablename__ = 'user_invitations'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, index=True,
                     server_default='', nullable=False)
    email = db.Column(db.String(32), unique=True, index=True,
                      server_default='', nullable=False)
    token = db.Column(db.String(32), unique=True, index=True,
                      server_default='', nullable=False)
    sent_by = db.Column(db.Integer, db.ForeignKey('users.id'),
                        server_default='0', nullable=False)
    sent_on = db.Column(db.DateTime, nullable=False)
    accepted = db.Column(db.Boolean, nullable=False, default=False)
    accepted_on = db.Column(db.DateTime, nullable=True)

    @staticmethod
    def generate_invitation_token():
        chars = string.ascii_letters + string.digits + '+/'
        return ''.join(chars[ord(os.urandom(1)) % len(chars)] for _ in range(15))


class Project(db.Model):
    """Database model for a project."""
    __tablename__ = 'projects'

    id = id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, index=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                         server_default='0', nullable=False)
    created = db.Column(db.DateTime, nullable=False)
    uses_main_platform = db.Column(db.Boolean, nullable=False, default=False)
    owner = db.relationship('User', backref='project')
    platform = db.relationship('Platform', backref='project', lazy='dynamic')
    proxy = db.relationship('PlatformProxy', backref='project', lazy='dynamic')
    api_keys = db.relationship('APIKey', backref='project', lazy='dynamic')


class Platform(db.Model):
    """Database model for an agent platform."""
    __tablename__ = 'platforms'

    id = id = db.Column(db.Integer, primary_key=True)
    secret = db.Column(db.String(64), unique=True, index=True,
                       server_default='', nullable=False)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'),
                           server_default='0', nullable=False)
    instance_id = db.Column(
        db.String(64), index=True, server_default='', nullable=False)
    ip_address = db.Column(
        db.String(40), index=True, nullable=False)
    eip_alloc_id = db.Column(
        db.String(64), index=True, server_default='', nullable=False)
    eip_assoc_id = db.Column(
        db.String(64), index=True, server_default='', nullable=False)

    @staticmethod
    def generate_secret():
        return hashlib.sha256(uuid.uuid4().bytes).hexdigest()


class PlatformProxy(db.Model):
    """Database model for an agent platform proxy server."""
    __tablename__ = 'platform_proxies'

    id = id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'),
                           server_default='0', nullable=False)
    instance_id = db.Column(
        db.String(64), index=True, server_default='', nullable=False)
    ip_address = db.Column(
        db.String(40), index=True, nullable=False)
    eip_alloc_id = db.Column(
        db.String(64), index=True, server_default='', nullable=False)
    eip_assoc_id = db.Column(
        db.String(64), index=True, server_default='', nullable=False)


class Flag(db.Model):
    """Database model for a system flag."""
    __tablename__ = 'flags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, index=True,
                     server_default='', nullable=False)
    description = db.Column(db.String(64))
    value = db.Column(db.Boolean, nullable=False, default=False)
    last_modified = db.Column(db.DateTime, nullable=True)


class APIKey(db.Model):
    """Database model for an API key."""
    __tablename__ = 'api_keys'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, index=True,
                     server_default='', nullable=False)
    key = db.Column(db.String(64), unique=True, index=True,
                    server_default='', nullable=False)
    created = db.Column(db.DateTime, nullable=False)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'),
                           server_default='0', nullable=False)

    @staticmethod
    def generate_api_key():
        return hashlib.sha256(uuid.uuid4().bytes).hexdigest()
