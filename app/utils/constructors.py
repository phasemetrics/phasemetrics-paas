from flask import (
    request,
    url_for,
)

from .models import (
    Flag,
    Project,
)


def view_project(*args, **kwargs):
    pid = request.view_args['pid']
    p = Project.query.filter_by(id=pid).first()
    crumbs = [
        {'text': p.name, 'url': url_for('project.view', pid=pid)}
    ]
    return crumbs


def edit_project(*args, **kwargs):
    pid = request.view_args['pid']
    p = Project.query.filter_by(id=pid).first()
    crumbs = [
        {'text': p.name, 'url': url_for('project.view', pid=pid)},
        {'text': 'Edit'}
    ]
    return crumbs


def edit_flag(*args, **kwargs):
    fid = request.view_args['fid']
    flag = Flag.query.filter_by(id=fid).first()
    crumbs = [
        {'text': 'Edit ' + flag.name}
    ]
    return crumbs


def project_add_api_key(*args, **kwargs):
    pid = request.view_args['pid']
    p = Project.query.filter_by(id=pid).first()
    crumbs = [
        {'text': p.name, 'url': url_for('project.view', pid=pid)},
        {'text': 'Add API Key'}
    ]
    return crumbs
