"""Interface with the linode API."""

import os
import requests


class LinodeWrapper(object):

    def __init__(self):
        self.key = os.environ.get('LINODE_API_KEY')
        self.endpoint = 'https://api.linode.com'

    def api_command(self, action, data={}):
        data.update({
            'api_key': self.key,
            'api_action': action
        })
        response = requests.get(self.endpoint, data)
        if response.status_code is 200:
            return response.json()
        return None

    # Linode.
    def boot(self, LinodeID, ConfigID=None):
        data = {
            'LinodeID': LinodeID
        }
        if ConfigID is not None:
            data['ConfigID'] = ConfigID
        return self.api_command('linode.boot', data)

    def clone(self, LinodeID, DataCenterID, PlanID, PaymentTerm=None):
        data = {
            'LinodeID': LinodeID,
            'DatacenterID': DataCenterID,
            'PlanID': PlanID
        }
        if PaymentTerm is not None:
            data['PaymentTerm'] = PaymentTerm
        return self.api_command('linode.clone', data)

    def create(self, DatacenterID, PlanID, PaymentTerm=None):
        data = {
            'DatacenterID': DatacenterID,
            'PlanID': PlanID
        }
        if PaymentTerm is not None:
            data['PaymentTerm'] = PaymentTerm
        return self.api_command('linode.create', data)

    def delete(self, LinodeID, skipChecks=None):
        data = {
            'LinodeID': LinodeID
        }
        if skipChecks is not None:
            data['skipChecks'] = skipChecks
        return self.api_command('linode.delete', data)

    def list(self, LinodeID=None):
        data = {}
        if LinodeID is not None:
            data['LinodeID'] = LinodeID
        return self.api_command('linode.list', data)

    def reboot(self, LinodeID, ConfigID=None):
        data = {
            'LinodeID': LinodeID
        }
        if ConfigID is not None:
            data['ConfigID'] = ConfigID
        return self.api_command('linode.reboot', data)

    def resize(self, LinodeID, PlanID):
        data = {
            'LinodeID': LinodeID,
            'PlanID': PlanID
        }
        return self.api_command('linode.resize', data)

    def shutdown(self, LinodeID):
        data = {
            'LinodeID': LinodeID
        }
        return self.api_command('linode.shutdown', data)

    def update(self, LinodeID, **kwargs):
        data = {
            'LinodeID': LinodeID
        }
        data.update(kwargs)
        return self.api_command('linode.update', data)

    def config_create(self, LinodeID, KernelID, Label, DiskList, **kwargs):
        data = {
            'LinodeID': LinodeID,
            'KernelID': KernelID,
            'Label': Label,
            'DiskList': DiskList
        }
        data.update(kwargs)
        return self.api_command('linode.config.create', data)

    def config_delete(self, LinodeID, ConfigID):
        data = {
            'LinodeID': LinodeID,
            'ConfigID': ConfigID
        }
        return self.api_command('linode.config.delete', data)

    def config_list(self, LinodeID, ConfigID=None):
        data = {
            'LinodeID': LinodeID
        }
        if ConfigID is not None:
            data['ConfigID'] = ConfigID
        return self.api_command('linode.config.list', data)

    def config_update(self, ConfigID, **kwargs):
        data = {
            'ConfigID'
        }
        data.update(kwargs)
        return self.api_command('linode.config.update', data)

    def disk_create(self, LinodeID, Label, Type, Size, **kwargs):
        data = {
            'LinodeID': LinodeID,
            'Label': Label,
            'Type': Type,
            'Size': Size
        }
        data.update(kwargs)
        return self.api_command('linode.disk.create', data)

    def disk_createfromimage(self, ImageID, LinodeID, **kwargs):
        data = {
            'ImageID': ImageID,
            'LinodeID': LinodeID
        }
        data.update(kwargs)
        return self.api_command('linode.disk.createfromimage', data)

    def disk_delete(self, LinodeID, DiskID):
        data = {
            'LinodeID': LinodeID,
            'DiskID': DiskID
        }
        return self.api_command('linode.disk.delete', data)

    def disk_list(self, LinodeID, DiskID=None):
        data = {
            'LinodeID': LinodeID
        }
        if DiskID is not None:
            data['DiskID'] = DiskID
        return self.api_command('linode.disk.list', data)

    def ip_addprivate(self, LinodeID):
        data = {
            'LinodeID': LinodeID
        }
        return self.api_command('linode.ip.addprivate', data)

    def ip_addpublic(self, LinodeID):
        data = {
            'LinodeID': LinodeID
        }
        return self.api_command('linode.ip.addpublic', data)

    def ip_list(self, **kwargs):
        return self.api_command('linode.ip.list', kwargs)

    def ip_setrdns(self, IPAddressID, Hostname):
        data = {
            'IPAddressID': IPAddressID,
            'Hostname': Hostname
        }
        return self.api_command('linode.ip.setrdns', data)

    def ip_swap(self, IPAddressID, **kwargs):
        data = {
            'IPAddressID': IPAddressID
        }
        data.update(kwargs)
        return self.api_command('linode.ip.swap', data)

    # Image.
    def image_delete(self, ImageID):
        data = {
            'ImageID': ImageID
        }
        return self.api_command('image.delete', data)

    def image_list(self, **kwargs):
        return self.api_command('image.list', kwargs)

    def image_update(self, ImageID, **kwargs):
        data = {
            'ImageID': ImageID
        }
        data.update(kwargs)
        return self.api_command('image.update', data)

    # Helper functions.
    def get_dict(self, l, value):
        for d in l['DATA']:
            if value in d.values():
                return d
        return None

    def platform_create(self, dcid, pid):
        linode = self.create(10, 1)
        if 'LinodeID' in linode['DATA']:
            ip = self.ip_list(LinodeID=linode['DATA']['LinodeID'])
            if ip is None:
                return None

            image = self.get_dict(self.image_list(), 'JADE Master')
            if image is None:
                return None

            swap = self.disk_create(
                linode['DATA']['LinodeID'], 'Swap', 'swap', 4096)
            if swap is None:
                return None

            disk = self.disk_createfromimage(image['IMAGEID'],
                                             linode['DATA']['LinodeID'])
            if disk is None:
                return None

            config = self.config_create(linode['DATA']['LinodeID'], 138,
                                        'PhaseMetrics',
                                        [disk['DATA']['DISKID'],
                                         swap['DATA']['DiskID']],
                                        RootDeviceNum=1)
            if config is None:
                return None
        else:
            print(linode)
            return None

        platform = {
            'machine': linode['DATA'],
            'ip': ip['DATA'][0],
            'image': image,
            'swap': swap['DATA'],
            'disk': disk['DATA'],
            'config': config['DATA']
        }
        return platform

    def platform_get_data(self, p):
        linode = self.list(p.machine_id)
        ip = self.ip_list(LinodeID=p.machine_id)
        #image = self.image_list(ImageID=p.image_id)
        #swap = self.disk_list(p.machine_id, p.disk_id)
        #disk = self.disk_list(p.machine_id, p.disk_id)
        #config = self.config_list(p.machine_id, p.config_id)

        platform = {
            'machine': linode['DATA'][0],
            'ip': ip['DATA'][0],
            #   'image': image['DATA'][0],
            #   'swap': swap['DATA'][0],
            #   'disk': disk['DATA'][0],
            #   'config': config['DATA'][0]
        }
        return platform

    def platform_get_main(self, name):
        linode = self.list()
        machine = self.get_dict(linode, name)
        ip = self.ip_list(LinodeID=machine['LINODEID'])

        platform = {
            'machine': machine,
            'ip': ip['DATA'][0],
            'image': 0,
            'swap': 0,
            'disk': 0,
            'config': 0
        }
        return platform
