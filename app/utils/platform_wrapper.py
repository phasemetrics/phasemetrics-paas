"""Interface with a project's AgentPlatform via the RESTAgent"""


import requests
from requests.exceptions import (
    ConnectionError, ConnectTimeout,
)


class AgentPlatformWrapper(object):

    def __init__(self, ip_address):
        self.endpoint = 'http://{0}:5001/api/'.format(ip_address)

    def is_up(self):
        status = False
        try:
            requests.get(self.endpoint, timeout=1)
            status = True
        except ConnectTimeout:
            pass
        except ConnectionError:
            pass

        return status

    def api_command(self, path):
        response = requests.get('{0}{1}'.format(self.endpoint, path))
        if response.status_code is 200:
            return response.json()
        return none

    def api_agents(self, agent_id=None):
        path = 'agents'
        if agent_id is not None:
            path = '{0}/{1}'.format(path, agent_id)
        return self.api_command(path)

    def api_containers(self, container_id=None):
        path = 'containers'
        if container_id is not None:
            path = '{0}/{1}'.format(path, container_id)
        return self.api_command(path)
