"""Flask-Script commands."""

from flask_migrate import (
    init,
    migrate,
    upgrade,
)
from flask_script import Command

from app import db
from app.utils.models import (
    Flag,
    Role,
    User,
)


class InitDbCommand(Command):

    def run(self):
        init()
        migrate()
        upgrade()


class CreateAdminCommand(Command):

    def run(self):
        Role.insert_roles()
        User.insert_admin_user()


class InstallCommand(Command):

    def run(self):
        flag = Flag(
            name='invitation_only',
            description='Registration is by invitation only.',
            value=True
        )
        db.session.add(flag)
        db.session.commit()
