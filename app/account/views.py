"""Implements account ui endpoints."""

from flask import (
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import (
    current_user,
    fresh_login_required,
)

from app import db
from app.account import account
from app.account.forms import AccountSettingsForm
from app.utils.mail import send_mail


@account.route('/settings', methods=['GET', 'POST'])
@fresh_login_required
def settings():
    """Present form to edit user details."""
    form = AccountSettingsForm()
    if form.validate_on_submit():
        if form.username.data != current_user.username:
            current_user.username = form.username.data

        if form.email.data != current_user.email:
            current_user.email = form.email.data
            current_user.confirmed = False
            token = current_user.generate_security_token()
            confirm_url = url_for('auth.confirm_email',
                                  token=token, _external=True)
            text = render_template('mail/confirmation-change.txt', vars={
                'user': current_user,
                'confirm_url': confirm_url
            })
            html = render_template('mail/confirmation-change.html', vars={
                'user': current_user,
                'confirm_url': confirm_url
            })
            subject = 'Please confirm your new email address.'
            send_mail(subject, recipients=[current_user.email],
                      text=text, html=html)

            flash('You need to confirm your new email address.', 'warning')
            flash('A confirmation email has been sent to {0}'
                  .format(current_user.email), 'info')

        if form.new_password.data != '':
            current_user.password = form.new_password.data

        db.session.add(current_user)
        db.session.commit()
        flash('User details updated.', 'success')
        return redirect(request.path)
    form.username.data = current_user.username
    form.email.data = current_user.email

    page_vars = {
        'title': 'Account Settings',
        'page_header': 'Account Settings',
        'form': form
    }
    return render_template('account/accountsettings.html', vars=page_vars)
