"""Implements forms used in the account ui."""

from flask.ext.login import current_user
from flask.ext.wtf import Form
from wtforms import (
    PasswordField,
    StringField,
    SubmitField,
    )
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    EqualTo,
    Length,
    optional,
    Required,
    ValidationError,
    )

from ..utils.models import User


class AccountSettingsForm(Form):
    """Form used to edit user details."""
    username = StringField('Username', validators=[Required(), Length(5, 32)])
    email = EmailField('Email address', validators=[Required(), Email()])
    password = PasswordField('Current Password', validators=[Required()],
                             description='You must supply your current \
                                          password to update your details.')
    new_password = PasswordField(
        'New Password', validators=[Length(8, 64), optional()])
    confirm_new_password = PasswordField(
        'Confirm New Password',
        validators=[EqualTo(
            'new_password',
            "Field must be equal to New Password"), optional()])
    submit = SubmitField('Submit Account Changes')

    @staticmethod
    def validate_username(dummy_form, field):
        """Check that the supplied username does not exist in the system."""
        user = User.query.filter_by(username=field.data).first()
        if user is not None and user.id != current_user.id:
            raise ValidationError('Username taken.')

    @staticmethod
    def validate_email(dummy_form, field):
        """Check that the supplied email does not exist in the system."""
        user = User.query.filter_by(email=field.data).first()
        if user is not None and user.id != current_user.id:
            raise ValidationError('There is already an account associated \
                                  with the supplied email address.')

    @staticmethod
    def validate_password(dummy_form, field):
        """Verify current password before updating details."""
        if not current_user.verify_password(field.data):
            raise ValidationError('Incorrect password.')
