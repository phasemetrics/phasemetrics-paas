"""Implements user account ui."""

from flask import Blueprint

account = Blueprint('account', __name__)

from . import (
    forms,
    views,
    )

__all__ = ['account']
