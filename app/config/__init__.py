"""Implements the configuration ui."""

from flask import Blueprint

config = Blueprint('config', __name__)

from . import (
    forms,
    views,
)

__all__ = ['config']
