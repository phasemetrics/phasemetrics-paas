"""Implements forms used in the configuration ui."""

from flask_wtf import Form

from wtforms import (
    BooleanField,
    StringField,
    SubmitField,
    ValidationError,
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    Length,
    Required,
)

from app.utils.models import (
    User,
)


class FlagsForm(Form):
    submit = SubmitField('Save Changes')


class AddFlagForm(Form):
    name = StringField('Name', validators=[Length(max=32)])
    description = StringField(
        'Description', validators=[Length(max=64)])
    value = BooleanField('value')
    submit = SubmitField('Create Flag')


class EditFlagForm(Form):
    name = StringField('Name', validators=[Length(max=32)])
    description = StringField(
        'Description', validators=[Length(max=64)])
    value = BooleanField('value')
    submit = SubmitField('Save Changes')


class InviteUserForm(Form):
    name = StringField('Name', validators=[Required()])
    email = EmailField('Email address', validators=[Required(), Email()])
    submit = SubmitField('Send Invitation')

    @staticmethod
    def validate_email(dummy_form, field):
        """Check that the supplied email does not exist in the system."""
        user = User.query.filter_by(email=field.data).first()
        if user is not None:
            raise ValidationError('There is already an account associated \
                                  with the supplied email address.')
