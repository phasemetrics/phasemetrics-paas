"""Implements the core API."""

from flask import Blueprint

api = Blueprint('api_v1_0', __name__)

from app.api_v1_0 import views

__all__ = ['api']