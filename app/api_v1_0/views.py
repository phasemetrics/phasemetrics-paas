"""Implements endpoints for the core API."""

from flask import (
    current_app,
    g,
    jsonify,
    request,
)

from itsdangerous import (
    BadSignature,
    URLSafeTimedSerializer,
)

from app.api_v1_0 import api
from app.utils.models import (
    APIKey,
    Platform,
    Project,
)


def after_this_request(f):
    if not hasattr(g, 'after_request_callbacks'):
        g.after_request_callbacks = []
    g.after_request_callbacks.append(f)
    return f


@api.after_request
def call_after_request_callbacks(response):
    for callback in getattr(g, 'after_request_callbacks', ()):
        callback(response)
    return response


@api.route('/app/auth/<string:mk>/<string:ak>', methods=['GET'])
def app_auth(mk, ak):
    """Generate platform authentication token."""
    # Check that authentication is being requested by the master app.
    # Plugins should never request authentication directly.
    mkey = APIKey.query.filter_by(key=mk).first()
    if mkey is None:
        return jsonify({'data': {'Error': 'Forbidden'}}), 403

    mp = Project.query.filter_by(id=mkey.project_id).first()
    if mp is None or mp.owner_id != 1:
        return jsonify({'data': {'Error': 'Forbidden'}}), 403

    # Validate the plugin key that is submitted by the master app.
    key = APIKey.query.filter_by(key=ak).first()
    if key is None:
        return jsonify({'data': {'Error': 'Authentication Failure'}}), 401

    p = Project.query.filter_by(id=key.project_id).first()
    plt = p.platform.first()
    if plt is None:
        return jsonify({'data': {'Error': 'Bad Request'}}), 400

    s = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])

    # Return a token to the master app that will be passed back to the plugin.
    return jsonify({'data': {'token': s.dumps(plt.id, salt=plt.secret)}}), 200


@api.route('/app/auth/confirm/<string:ak>/<string:token>', methods=['GET'])
def app_auth_confirm(ak, token):
    """Validate platform authentication token."""
    # Reject validation if the request is coming from an unknown ip address.
    # i.e. an ip address with no corresponding platform in the system.
    plt = Platform.query.filter_by(ip_address=request.remote_addr).first()
    if plt is None:
        return jsonify({'data': {'Error': 'Forbidden'}}), 403

    # Ensure that the platform is not orphaned.
    # This is not really likely, but hey, being paranoid is not always bad.
    p = Project.query.filter_by(id=plt.project_id).first()
    if p is None:
        return jsonify({'data': {'Error': 'Forbidden'}}), 403

    s = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        token_key = s.loads(token, salt=plt.secret, max_age=30)
    except (BadSignature):
        return jsonify({'data': {'Error': 'Authentication Failure'}}), 401

    return jsonify({'data': {'Success': 'Authenticated'}}), 200
