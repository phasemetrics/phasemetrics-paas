#!/bin/bash
chmod -x /etc/update-motd.d/*
cat /dev/null > /etc/legal
useradd {{ username }}; echo -e "{{ password }}\n{{ password }}" | passwd {{ username }}
usermod -s /bin/false {{ username }}
usermod -d /home {{ username }}
sed -i 's/PrintLastLog yes/PrintLastLog no/' /etc/ssh/sshd_config
echo -e "\nGatewayPorts yes\nMatch User {{ username }}\n\tPasswordAuthentication yes" >> /etc/ssh/sshd_config
/etc/init.d/ssh restart
shutdown -r now