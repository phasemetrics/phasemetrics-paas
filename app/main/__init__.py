"""Implements the main ui."""

from flask import Blueprint

main = Blueprint('main', __name__)

from . import (
    errors,
    views,
    )
from ..utils.models import Permission

__all__ = ['main']


@main.app_context_processor
def inject_permissions():
    """Make permissions available in all template files."""
    return dict(Permission=Permission)
