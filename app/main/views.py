"""Implements main endpoints."""

from flask import (
    flash,
    redirect,
    render_template,
    url_for,
)
from flask_breadcrumbs import register_breadcrumb

from app.main import main
from app.main.forms import ContactForm
from app.utils.mail import send_mail


@main.route('/')
@register_breadcrumb(main, 'breadcrumbs.', 'Home')
def index():
    """Render index page."""
    page_vars = {
        'jumbotron': True,
        'title': 'Welcome to PhaseMetrics'
    }
    return render_template('main/index.html', vars=page_vars)


@main.route('/products')
def products():
    """Render Products page."""
    page_vars = {
        'title': 'PhaseMetrics: Products'
    }
    return render_template('main/products.html', vars=page_vars)


@main.route('/products/hi')
def product_hi():
    """Render product page for PhaseMetrics HI"""
    page_vars = {
        'sidebar_class': ' sidebar',
        'jumbotron': True,
        'sidebar_right': True,
        'title': 'PhaseMetrics HI',
        'page_header': 'PhaseMetrics HI'
    }
    return render_template('main/product_hi.html', vars=page_vars)


@main.route('/products/android')
def product_droid():
    """Render product page for PhaseMetrics Android"""
    page_vars = {
        'jumbotron': True,
        'title': 'PhaseMetrics Android',
        'page_header': 'PhaseMetrics Android'
    }
    return render_template('main/product_droid.html', vars=page_vars)


@main.route('/services')
def services():
    """Render Services page."""
    page_vars = {
        'title': 'Page Not Found',
        'page_header': 'Page Not Found'
    }

    return render_template('error/404.html', vars=page_vars), 404


@main.route('/developers')
@register_breadcrumb(main, 'breadcrumbs.docs', 'Developer Documentation')
def developers():
    """Render Developers page."""
    page_vars = {
        'title': 'PhaseMetrics: Developer Information'
    }
    return render_template('main/developers.html', vars=page_vars)


@main.route('/developers/docs')
@register_breadcrumb(main, 'breadcrumbs.docs.intro', 'Introduction')
def developer_docs():
    """Render developer documentation home page."""
    page_vars = {
        'sidebar_class': ' sidebar',
        'title': 'Developer Documentation',
        'navwell': True,
        'page_header': 'Developer Documentation',
        'sidebar_left': True
    }
    return render_template('main/developer_docs.html', vars=page_vars)


@main.route('/contact', methods=['GET', 'POST'])
def contact():
    """Render Contact page."""
    form = ContactForm()

    if form.validate_on_submit():
        mail_vars = {
            'name': 'Admin',
            'site_name': 'PhaseMetrics.io',
            'from': '{0} ({1})'.format(form.name.data, form.email.data),
            'msg': form.msg.data
        }
        text = render_template('mail/site-contact.txt', vars=mail_vars)
        html = render_template('mail/site-contact.html', vars=mail_vars)

        subject = 'Contact via PhaseMetrics.io'
        cc = form.email.data if form.cc.data else None
        send_mail(subject, recipients=['admin@phasemetrics.io'],
                  cc=[cc], text=text, html=html, reply_to=form.email.data)

        flash('Your message has been sent. ' +
              'Thank you for contacting PhaseMetrics.', 'success')
        return redirect(url_for('main.contact'))

    page_vars = {
        'title': 'PhaseMetrics: Contact Us',
        'page_header': 'Contact Us',
        'form': form
    }
    return render_template('main/contact.html', vars=page_vars)
