"""Implements forms used in the main Blueprint ui."""

from flask_wtf import Form

from wtforms import (
    BooleanField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.fields.html5 import (
    EmailField,
)
from wtforms.validators import (
    Required,
)


class ContactForm(Form):
    name = StringField('name', validators=[Required()])
    email = EmailField('email', validators=[Required()])
    msg = TextAreaField('message', validators=[Required()])
    cc = BooleanField('cc myself')
    submit = SubmitField('Send')
