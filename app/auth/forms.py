"""Implements forms used in the authentication ui."""

from flask_wtf import Form
from wtforms import (
    BooleanField,
    HiddenField,
    PasswordField,
    StringField,
    SubmitField,
)
from wtforms.fields.html5 import (
    EmailField,
)
from wtforms.validators import (
    Email,
    EqualTo,
    Length,
    Required,
    ValidationError,
)

from app.utils.models import (
    Flag,
    User,
    UserInvitation,
)


class LoginForm(Form):
    """Form used too login an existing user."""
    username = StringField('Username', validators=[Required()])

    password = PasswordField('Password', validators=[Required()])

    remember_me = BooleanField('Keep me logged in')

    submit = SubmitField('Login')


class RegistrationForm(Form):
    """Form used to register a new account."""
    username = StringField('Username', validators=[
        Required(),
        Length(min=5, max=32)])

    email = EmailField('Email address', validators=[Required(), Email()])

    confirm_email = EmailField('Confirm Email address',
                               validators=[
                                   Required(),
                                   Email(),
                                   EqualTo('email',
                                           'The email fields must match.')
                               ])

    password = PasswordField('Password', validators=[Required()])

    token = StringField('Invite Token')

    submit = SubmitField('Create Account')

    @staticmethod
    def validate_username(dummy_form, field):
        """Check that the supplied username does not exist in the system."""
        user = User.query.filter_by(username=field.data).first()
        if user is not None:
            raise ValidationError('Username taken.')

    @staticmethod
    def validate_email(dummy_form, field):
        """Check that the supplied email does not exist in the system."""
        user = User.query.filter_by(email=field.data).first()
        if user is not None:
            raise ValidationError('There is already an account associated \
                                  with the supplied email address.')

    @staticmethod
    def validate_token(dummy_form, field):
        """Ensure the supplied invitation token is valid.
        This is only needed if there is an invitation_only flag exists and is
        set to true. Registration, however, should still be rejected if an
        invalid token is supplied.
        """
        io = Flag.query.filter_by(name='invitation_only').first()

        if field.data == '' and io:
            raise ValidationError('Registration is by invitation only. Please \
                                    supply a valid invite token.')
            return

        inv = UserInvitation.query.filter_by(token=field.data).first()
        if field.data != '' and (inv is None or inv.accepted):
            raise ValidationError('Please supply a valid invite token.')


class PasswordForm(Form):
    """Form used to request a password reset link."""
    email = EmailField('Email address', validators=[Required(), Email()])

    submit = SubmitField('Reset Password')

    @staticmethod
    def validate_email(dummy_form, field):
        """Check that the email exists in the system."""
        user = User.query.filter_by(email=field.data).first()
        if user is None:
            raise ValidationError('The supplied email address is not \
                                  associated with any accounts.')


class PasswordResetForm(Form):
    """Form used to set a new password."""
    reset_token = HiddenField('Reset Token')
    password = PasswordField('New Password', validators=[Required()])
    confirm_password = PasswordField('Confirm New Password', validators=[
        Required()
    ])
    submit = SubmitField('Set Password')

    @staticmethod
    def validate_confirm_password(form, field):
        """Check that password fields match."""
        if field.data != form.password.data:
            raise ValidationError('Passwords must match.')
