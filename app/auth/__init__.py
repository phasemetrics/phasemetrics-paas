"""Implements authentication ui."""

from flask import Blueprint

auth = Blueprint('auth', __name__)

from . import (
    forms,
    views,
)

__all__ = ['auth']
