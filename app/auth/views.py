"""Implements authentication ui endpoints."""

from datetime import datetime

from flask import (
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user,
)
from itsdangerous import BadSignature

from app import db
from app.auth import auth
from app.auth.forms import (
    LoginForm,
    PasswordForm,
    PasswordResetForm,
    RegistrationForm,
)
from app.utils.mail import send_mail
from app.utils.models import (
    Role,
    User,
    UserInvitation,
)


@auth.route('/register', methods=['GET', 'POST'])
def register():
    """Register a user account."""
    if current_user.is_authenticated():
        return redirect(url_for('main.index'))

    reg_form = RegistrationForm(prefix='reg_form')
    token = request.args.get('token')

    if reg_form.validate_on_submit():
        user = User(
            username=reg_form.username.data,
            email=reg_form.email.data,
            init=reg_form.email.data,
            password=reg_form.password.data,
            registered_on=datetime.now()
        )
        user.role = Role.query.filter_by(permissions=0x01).first()
        db.session.add(user)

        token = user.generate_security_token()
        confirm_url = url_for('auth.confirm_email',
                              token=token, _external=True)
        text = render_template('mail/confirmation.txt', vars={
            'user': user,
            'confirm_url': confirm_url
        })
        html = render_template('mail/confirmation.html', vars={
            'user': user,
            'confirm_url': confirm_url
        })
        subject = 'Please confirm your account registration.'
        send_mail(subject, recipients=[user.email], text=text, html=html)

        if reg_form.token.data is not '':
            inv = UserInvitation.query.filter_by(
                token=reg_form.token.data).first()
            inv.accepted = True
            inv.accepted_on = datetime.now()
            db.session.add(inv)

        db.session.commit()

        login_user(user)
        flash('Welcome! A confirmation email has been sent to {0}'
              .format(user.email), 'info')
        return redirect(url_for('main.index'))

    if token is not None:
        reg_form.token.data = token

    page_vars = {
        'sidebar_right': True,
        'sidebar_class': ' two-column',
        'title': 'Register an Account',
        'login_form': LoginForm(prefix='login_form'),
        'reg_form': reg_form
    }
    return render_template('auth/login.html', vars=page_vars)


@auth.route('/confirm/<token>')
@login_required
def confirm_email(token):
    """Corfirm account validation token."""
    try:
        current_user.confirm_security_token(token, max_age=86400)
    except BadSignature:
        flash('The confirmation link is invalid or has expired.', 'danger')
        return redirect(url_for('auth.unconfirmed'))

    if current_user.confirmed:
        flash('Account already confirmed.', 'info')
    else:
        current_user.confirmed = True
        current_user.confirmed_on = datetime.now()
        db.session.add(current_user)
        db.session.commit()
        flash('Account confirmation successful!', 'success')
    return redirect(url_for('auth.unconfirmed'))


@auth.route('/unconfirmed')
@login_required
def unconfirmed():
    """Prompt user to confirm account."""
    if current_user.confirmed:
        return redirect(url_for('main.index'))
    flash('Please confirm your account.', 'warning')

    page_vars = {
        'title': 'Confirm Your Account'
    }
    return render_template('auth/unconfirmed.html')


@auth.route('/resend-confirmation')
@login_required
def resend_confirmation():
    """Resend the account confirmation URL."""
    token = current_user.generate_security_token()
    confirm_url = url_for('auth.confirm_email', token=token, _external=True)
    text = render_template('mail/confirmation.txt', vars={
        'user': current_user,
        'confirm_url': confirm_url
    })
    html = render_template('mail/confirmation.html', vars={
        'user': current_user,
        'confirm_url': confirm_url
    })
    subject = 'Please confirm your account registration.'
    send_mail(subject, recipients=[current_user.email], text=text, html=html)

    flash('A confirmation email has been sent to {0}'
          .format(current_user.email), 'success')
    return redirect(url_for('main.index'))


@auth.route('/login', methods=['GET', 'POST'])
def login():
    """Present user login form."""
    if current_user.is_authenticated():
        return redirect(url_for('main.index'))

    login_form = LoginForm(prefix='login_form')

    if login_form.validate_on_submit():
        user = User.query.filter_by(username=login_form.username.data).first()
        if user is not None and user.verify_password(
                login_form.password.data) and user.active:
            login_user(user, login_form.remember_me.data)
            return redirect(request.args.get('next') or url_for('project.index'))
        flash('Invalid username or password.', 'danger')

    page_vars = {
        'sidebar_right': True,
        'sidebar_class': ' two-column',
        'title': 'Sign in',
        'login_form': login_form,
        'reg_form': RegistrationForm(prefix='reg_form')
    }
    return render_template('auth/login.html', vars=page_vars)


@auth.route('/logout')
@login_required
def logout():
    """Logout user."""
    logout_user()
    flash('You have been logged out.', 'info')
    return redirect(url_for('main.index'))


@auth.route('/password', methods=['GET', 'POST'])
def password():
    """Present password reset form."""
    pass_form = PasswordForm()

    if pass_form.validate_on_submit():
        user = User.query.filter_by(email=pass_form.email.data).first()
        token = user.generate_security_token()
        reset_url = url_for('auth.reset_password', token=token, _external=True)
        text = render_template('mail/password.txt', vars={
            'user': user,
            'reset_url': reset_url
        })
        html = render_template('mail/password.html', vars={
            'user': user,
            'reset_url': reset_url
        })
        subject = 'Password reset instructions.'
        send_mail(subject, recipients=[user.email], text=text, html=html)

        flash('Password reset instructions have been sent to {0}'
              .format(user.email), 'success')
        return redirect(url_for('auth.login'))

    page_vars = {
        'title': 'Reset Your Password',
        'page_header': 'Reset your password',
        'form': pass_form
    }
    return render_template('auth/password.html', vars=page_vars)


@auth.route('/password/reset/<token>', methods=['GET', 'POST'])
def reset_password(token):
    """Corfirm password reset token."""
    try:
        email = User.confirm_password_reset_token(token)
    except BadSignature:
        flash('The password reset link is invalid or has expired.', 'danger')
        return redirect(url_for('main.index'))

    pass_form = PasswordResetForm(reset_token=token)

    if pass_form.validate_on_submit():
        user = User.query.filter_by(email=email).first()
        if user is not None:
            try:
                user.confirm_security_token(pass_form.reset_token.data)
            except BadSignature:
                flash('The password reset link is invalid or has expired.',
                      'danger')
                return redirect(url_for('main.index'))

            user.password = pass_form.password.data
            db.session.add(user)
            db.session.commit()
            flash('Your password has been successfully reset. You can now \
                  login using your new password.', 'success')
            return redirect(url_for('auth.login'))

    page_vars = {
        'title': 'Set New Password',
        'page_header': 'Set your new password',
        'form': pass_form
    }
    return render_template('auth/new-password.html', vars=page_vars)
