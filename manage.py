#!/usr/bin/env python
"""Flask management script."""

import os

from flask_migrate import (
    Migrate,
    MigrateCommand,
)
from flask_script import (
    Manager,
    Shell,
)

from app import (
    create_app,
    get_app_prefix,
    db,
)
from app.utils.commands import (
    CreateAdminCommand,
    InitDbCommand,
    InstallCommand,
)
from app.utils.models import (
    Role,
    User,
)
import app.utils.context  # @UnusedImport Inject global template variables.

__all__ = ['app', 'manager', 'migrate']

app = create_app(os.getenv(get_app_prefix() + '_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    """Make object models available in the shell."""
    return dict(app=app, db=db, User=User, Role=Role)

manager.add_command('create-admin', CreateAdminCommand)
manager.add_command('db', MigrateCommand)
manager.add_command('initdb', InitDbCommand)
manager.add_command('do-install', InstallCommand)
manager.add_command('shell', Shell(make_context=make_shell_context))


@manager.command
def test():
    """Run the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

if __name__ == '__main__':
    manager.run()
