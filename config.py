""" Base configuration file for the Maziko app."""

import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))
APP_PREFIX = 'PHASEMETRICS'


class Config(object):
    """Base configuration class."""
    BASEDIR = BASEDIR

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    SECRET_KEY = os.environ.get(APP_PREFIX + '_SECRET_KEY')

    SECURITY_PASSWORD_SALT = os.environ.get(APP_PREFIX +
                                            '_SECURITY_PASSWORD_SALT')

    _ADMIN = os.environ.get(APP_PREFIX + '_ADMIN')
    _ADMIN_PASSWORD = os.environ.get(APP_PREFIX + '_ADMIN_PASSWORD')
    MAIL_SERVER = os.environ.get(APP_PREFIX + '_MAIL_SERVER')
    MAIL_PORT = os.environ.get(APP_PREFIX + '_MAIL_PORT')
    MAIL_USE_TLS = bool(os.environ.get(APP_PREFIX + '_MAIL_USE_TLS'))
    MAIL_USE_SSL = bool(os.environ.get(APP_PREFIX + '_MAIL_USE_SSL'))
    MAIL_USERNAME = os.environ.get(APP_PREFIX + '_MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get(APP_PREFIX + '_MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.environ.get(APP_PREFIX + '_MAIL_DEFAULT_SENDER')

    SITE_NAME = 'PhaseMetrics'
    ORG_NAME = 'PhaseMetrics, Inc.'

    AWS_MAINPLATFORM_NAME = 'PhaseMetrics Website'

    AWS_JADEPLATFORM_VERSION = 'master'
    AWS_JADEVPC_NAME = 'JADE VPC'
    AWS_JADEVPC_SECURITYGROUP = 'jade-platform'
    AWS_JADEVPC_KEYNAME = 'JADEPlatform'
    LOCAL_JADEVPC_KEY = '/var/jadeplatform/JADEPlatform.pem'
    JADEVPC_USER = 'ubuntu'

    AWS_JADEPROXY_VERSION = 'master'
    AWS_JADEPROXYVPC_NAME = 'JADE Proxies'
    AWS_JADEPROXYVPC_SECURITYGROUP = 'jade-proxy'
    AWS_JADEPROXYVPC_KEYNAME = 'JADEProxy'
    LOCAL_JADEPROXYVPC_KEY = '/var/jadeplatform/JADEProxy.pem'

    GA_TRACKING_ID = os.environ.get(APP_PREFIX + '_GA_TRACKING_ID')

    @staticmethod
    def init_app(app):
        """Configuration specific initialization."""
        pass


class DevelopmentConfig(Config):
    """Environment specific Configuration: Development."""
    LOG_LEVEL = 10
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get(APP_PREFIX + '_DEV_DATABASE_URL')\
        or 'sqlite:///' + \
        os.path.join(BASEDIR, 'storage', APP_PREFIX + '-dev.sqlite')


class TestingConfig(Config):
    """Environment specific Configuration: Testing."""
    LOG_LEVEL = 20
    TESTING = True
    SQLALCHEMY_DATABASE_URI = \
        os.environ.get(APP_PREFIX + '_TEST_DATABASE_URL') or 'sqlite:///' + \
        os.path.join(BASEDIR, 'storage', APP_PREFIX + '-test.sqlite')


class ProductionConfig(Config):
    """Environment specific Configuration: Production."""
    LOG_LEVEL = 30
    SQLALCHEMY_DATABASE_URI = os.environ.get(APP_PREFIX + '_DATABASE_URL') \
        or 'sqlite:///' + os.path.join(BASEDIR, 'storage',
                                       APP_PREFIX + '.sqlite')


# Configuration variable to be imported by other modules.
CONFIG = {
    'default': DevelopmentConfig,
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig
}
